## How to run  

+ before run the script you need to create virtual environment or use global environment  
+ install needed libraries  
+ run script with this command (don't forget to paste right credentials')
```bash
python main.py --docker-image python:3 --bash-command $'pip install pip -U && pip install tqdm && python -c \"import time\nfor i in range(25):\n\tprint(i)\n\ttime.sleep(0.1)\"' --aws-cloudwatch-group test-task-group-1 --aws-cloudwatch-stream test-task-stream-1 --aws-access-key-id XXX --aws-secret-access-key XXX --aws-region eu-central-1
```

It works well only without an endless loop and in a case without any interruption.  

I also tried to use this approach with [logging driver for CloudWatch from Docker](https://docs.docker.com/config/containers/logging/awslogs/):  
```python
log_config = LogConfig(type=LogConfig.types.JSON, config={
    "log-driver": "awslogs",
    "log-opts": {
        "awslogs-group": args.aws_cloudwatch_group,
        "awslogs-region": args.aws_region,
        "awslogs-stream": args.aws_cloudwatch_stream
    },
})
container = client.containers.run(image=image_name, command=command, detach=True, remove=True, log_config=log_config)
```
Unfortunately it wasn't successful, I had some error with credentials. But it seems like the right way to organize needed behavior. 

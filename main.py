import argparse
import datetime

import docker
import boto3
from botocore.exceptions import ClientError


def parse_arguments():
    parser = argparse.ArgumentParser(description="run a docker container and log its output to aws cloudwatch.")
    parser.add_argument("--docker-image", required=True, help="name of the docker image")
    parser.add_argument("--bash-command", required=True, help="bash command to run inside the docker image")
    parser.add_argument("--aws-cloudwatch-group", required=True, help="aws cloudwatch log group name")
    parser.add_argument("--aws-cloudwatch-stream", required=True, help="aws cloudwatch log stream name")
    parser.add_argument("--aws-access-key-id", required=True, help="aws access key ID")
    parser.add_argument("--aws-secret-access-key", required=True, help="aws secret access key")
    parser.add_argument("--aws-region", required=True, help="aws region")
    return parser.parse_args()


def create_cloudwatch_client(access_key, secret_key, region):
    return boto3.client(service_name='logs', aws_access_key_id=access_key, aws_secret_access_key=secret_key, region_name=region)


def ensure_cloudwatch_log_group(client, group_name):
    try:
        client.create_log_group(logGroupName=group_name)
    except ClientError as e:
        if e.response['Error']['Code'] != 'ResourceAlreadyExistsException':
            print(f'log group with name "{group_name}" already exists')
    except Exception:
        print('something went wrong')
        raise


def ensure_cloudwatch_log_stream(client, group_name, stream_name):
    try:
        client.create_log_stream(logGroupName=group_name, logStreamName=stream_name)
    except ClientError as e:
        if e.response['Error']['Code'] != 'ResourceAlreadyExistsException':
            print(f'log stream with name "{stream_name}" in group "{group_name}" already exists')
    except Exception:
        print('something went wrong')
        raise


def run_docker_container(image_name, bash_command):
    client = docker.from_env()
    command = f"bash -c '{bash_command}'"
    container = client.containers.run(image=image_name, command=command, detach=True, remove=True)
    return container


def main():
    args = parse_arguments()
    print(f'got arguments: {args}')

    cw_client = create_cloudwatch_client(args.aws_access_key_id, args.aws_secret_access_key, args.aws_region)

    ensure_cloudwatch_log_group(cw_client, args.aws_cloudwatch_group)
    ensure_cloudwatch_log_stream(cw_client, args.aws_cloudwatch_group, args.aws_cloudwatch_stream)

    container = run_docker_container(args.docker_image, args.bash_command)

    for line in container.logs(stream=True):
        try:
            print(line.decode('utf-8'))
            cw_client.put_log_events(
                logGroupName=args.aws_cloudwatch_group,
                logStreamName=args.aws_cloudwatch_stream,
                logEvents=[
                    {
                        'timestamp': int(datetime.datetime.now().timestamp() * 1000),
                        'message': line.decode('utf-8')
                    }
                ]
            )
        except ClientError as e:
            print(f'error sending logs to cloudwatch: {e}')


if __name__ == "__main__":
    main()
